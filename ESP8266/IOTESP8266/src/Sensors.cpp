
#include <OneWire.h>
#include <DallasTemperature.h>



static void printTemperature(DeviceAddress deviceAddress);
static void printAddress(DeviceAddress deviceAddress);


DeviceAddress  addressDS18B20;


OneWire onewire(D6);
// DS18B20 sensors object
DallasTemperature sensors(&onewire);




static void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

// function to print the temperature for a device
static void printTemperature(DeviceAddress deviceAddress)
{
  float tempC = sensors.getTempC(deviceAddress);
  Serial.print("Temp C: ");
  Serial.print(tempC);
  Serial.print(" Temp F: ");
  Serial.print(DallasTemperature::toFahrenheit(tempC));
}

float readTemp(void)
{
Serial.print("TEMPERATURA TO: ");
sensors.begin();
// locate devices on the bus
Serial.print("Found ");
Serial.print(sensors.getDeviceCount(), DEC);
Serial.println(" devices.");
sensors.getAddress(addressDS18B20, 0);
sensors.requestTemperatures();
printTemperature(addressDS18B20);

 float tempC = sensors.getTempC(addressDS18B20);
return tempC;
}



int readAnalogVal(uint8_t Pin)
{
pinMode(Pin , INPUT);
return analogRead(Pin);

}



void sensorsInit(void)
{


sensors.begin();

}