
#define DEBUG_ON
#define DEBUG_ESP_HTTP_CLIENT


#include "WebServ.h"

#include "Sensors.h"
 #include <stdio.h>

 #include "GenerateWebside.h"

 #include "MEMORY.h"
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

#include <EasyNTPClient.h>
#include <WiFiUdp.h>
#include <NTPtimeESP.h>
#include <ESP8266HTTPClient.h>





static void handleRequest();
static void devicesInfo();
static void handleSetDeviceConf();




HTTPClient http;
WiFiClient client;




ESP8266WebServer server; // obiekt serwera na wybranym (80) porcie


WiFiUDP udp;
EasyNTPClient ntpClient(udp, "pool.ntp.org", ((2*60*60))); // IST = GMT + 2



NTPtime NTPch("pool.ntp.org");   // Choose server pool as require



strDateTime dateTime;






void connectWifi(){

 WiFi.mode(WIFI_STA);
 Serial.println("Connect to:" +dataConfig.SSID+ " Pass" + dataConfig.PASSWORD );

const char * ssid = dataConfig.SSID.c_str();
const char * pass = dataConfig.PASSWORD.c_str();
  WiFi.begin (ssid, pass);
  uint8_t noWork = 0;

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(250);
    noWork ++;
//     if(noWork > 4*8)
//     {
//  WiFi.disconnect();
//  return; 
//     }
  }
  Serial.println("WiFi connected");




}


void getTimeFromNtp()
{


Serial.println(ntpClient.getUnixTime());

dateTime = NTPch.getNTPtime(1.0, 1);

  // check dateTime.valid before using the returned time
  // Use "setSendInterval" or "setRecvTimeout" if required
  if(dateTime.valid){
    NTPch.printDateTime(dateTime);
    Serial.print("WITAMY W ROKU");
    Serial.println(dateTime.year);
  }
  else{
     Serial.println("NO NTP");
  }


}




static void devicesInfo(){
 
 if(server.args()>0){
if(server.argName(0) == "LED1")
{
pinMode(D1,OUTPUT);
digitalWrite(D1,!digitalRead(D1));

}
if(server.argName(0) == "LED2")
{
  pinMode(D2,OUTPUT);
  digitalWrite(D2,!digitalRead(D2));
}
}


 float temp = readTemp();
 int valsens = readAnalogVal(A0);

 
server.sendContent("<body>");
server.sendContent("<div class=\"container\"> <div class=\"card temp\"> <div class=\"inner\"> <div class=\"icon\"></div> <div class=\"title\"> <div class=\"text\">TEMPERATURE</div> </div> <div class=\"number\"> ");
server.sendContent(String(temp, 2));
server.sendContent("</div> <div class=\"measure\">CELCIUS</div> </div> </div> <div class=\"card energy\"> <div class=\"inner innerE\"> <div class=\"iconE\"></div> <div class=\"title\"> <div class=\"text textE\">ENERGY</div> </div> <div class=\"number\">");
server.sendContent(String(valsens) )  ;
server.sendContent("</div> <div class=\"measure\">kW/HOUR</div> </div> </div> </div>");
server.sendContent("<form action=\"../temp\"><input  align=\"center\" name=\"LED1\" type=\"submit\" value =\"LED1\" ");
if(digitalRead(D1)){
  server.sendContent( " class=\"green but\" ");
}
else{
  server.sendContent( " class=\"red but\" ");
}

 server.sendContent("/> </br></form>");
server.sendContent("<form action=\"../temp\"><input align=\"center\" name=\"LED2\" type=\"submit\" value =\"LED2\" ");
if(digitalRead(D2)){
  server.sendContent( " class=\"green but\" ");
}
else{
  server.sendContent( " class=\"red but\" ");
}
 server.sendContent("/> </br> </form>");

   server.sendContent("</body>");   
server.sendContent("<style>"); 


server.sendContent(".container{ width: 385px; height: 226px; margin: 50px auto; } .card{ float: left; width: 180px; height: 100%; border: 1px solid; border-radius: 5px; margin-right: 10px; background: #F9F9F9; position: relative; box-shadow: 0px 0px 3px #ddd; }");
server.sendContent(" .inner{ background: #F9F9F9; border: 1px solid #E3E3E3; border-radius: 0px 0px 5px 5px; width: 100%; height: 100px; position: absolute; bottom: -1px; left: -1px;  font-family: arial; text-align: center; box-shadow: inset 0px 0px 1px #FFF; }");
server.sendContent(" .title{ display: block; font-size: 9px; color: #fff; margin-top: -10px;} .text{ display: inline-block; padding: 5px 15px; border-radius: 50px; font-weight: bold; }  .number{ color: #313A3E; font-size: 50px; font-weight: bold; display: block; margin-top: 5px; } ");
server.sendContent("  .measure{ color: #676767; font-size: 10px; display: block; margin-top: -10px; } .temp{ background:  #EC6F69; border: 1px solid  #DE3932;} .inner{ border-top: 1px solid  #DE3932;} .text{ border: 1px solid  #DE3932; background:  #EC6F69; text-shadow: 1px 1px 0px  #DE3932; } ");
server.sendContent(" .icon{ display: block; position: absolute; top: -65px; left: 75px; width: 17px; height: 17px; background: #fff; border-radius: 17px; border: 4px solid  #EC6F69; box-shadow: 0 0 0 4px #fff, 		1px 1px 0 4px  #DE3932; } ");
server.sendContent(" .icon:before{ content: ''; display: block; width: 7px; height: 25px; background: #fff; position: absolute; top: -30px; left: 0px; border: 5px solid  #EC6F69; border-radius: 10px 10px 0px 0px; border-bottom: 0px none; box-shadow: 0 -4px 0 4px #fff 		,4px -3px 0 1px  #DE3932; }  ");
server.sendContent(" .energy{ background: #A6C659; border: 1px solid #7DAD0A;} .innerE{ border-top: 1px solid #7DAD0A;} .iconE{ width: 53px; height: 53px; position: absolute; top: -100px; left: 60px; border: 5px solid #fff; border-radius: 65px; box-shadow: 1px 1px 0px #7DAD0A;} .iconE:before{ content: ''; display: block; border-right: 15px solid #fff; border-top: 40px solid transparent; transform: skew(-30deg,0); position: absolute; top: -10px; right: 15px; box-shadow: 5px -10px #A6C659; }");
server.sendContent(" .iconE:after{ content: ''; display: block; border-right: 15px solid #fff; border-bottom: 40px solid transparent; transform: skew(-40deg,0); position: absolute; top: 25px; right: 30px; box-shadow: 5px 0 0px #A6C659; } .textE{ border: 1px solid #7DAD0A; background: #A6C659; text-shadow: 1px 1px 0px #7DAD0A; } ");
server.sendContent(" .red{ background: #FF5555;} .green{ background: #55FF55;} .but{ margin:auto; display:block; align-item:center; color: #fff !important; text-transform: uppercase;  padding: 20px; border-radius: 50px; margin-top:1; border: none;}");
server.sendContent(".but:hover{ text-shadow: 0px 0px 6px rgba(255, 255, 255, 1); -webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57); -moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57); transition: all 0.4s ease 0s; }");

server.sendContent("</style>"); 

}

static void handleRequest()
{
generateWebLogin();
}


static void handleSetDeviceConf()
{
 Serial.print("DOSTEPNA ILOŚĆ ARGUMENTÓW :");
 Serial.println(server.args(),DEC);

  
 for(int i = 0 ; i < server.args() ; i ++)
 {
 

if(server.argName(i) == "WiFiAvalNet")
{
  Serial.println(server.arg(i) );
 Serial.println(server.argName(i));

dataConfig.SSID = server.arg(i);
}
else if(server.argName(i) == "password")
{
  Serial.println(server.arg(i) );
 Serial.println(server.argName(i));
dataConfig.PASSWORD = server.arg(i);
}
else if(server.argName(i) == "thingServ")
{
  Serial.println(server.arg(i) );
 Serial.println(server.argName(i));
dataConfig.ThingServ = server.arg(i);
}

}

Serial.println("Odczytane dane do zapisu:" + dataConfig.SSID + " " + dataConfig.PASSWORD);


memory_save();

server.sendContent("<body><a href=\"../../\">Zmieniono ustawienia wróć </a></body>");



}



void WebServerInit(void)
{

   if(digitalRead(D3))
  {

memory_ReadData();

Serial.print("WYBRAŁEŚ SIEĆ : " + dataConfig.SSID);
  Serial.println(" A hasło to :" + dataConfig.PASSWORD); 
connectWifi(); 
 //getTimeFromNtp();
  while(1){
  senddataToThingworx();

  //ESP.deepSleep(1e6*60*5); //5min
  ESP.deepSleep(1e6*10*1);
  }
  }
  else{

  //WiFi.begin(ssid, password); // połączenie do sieci
WiFi.softAP("ESP8266SERVER","qwerty1230",1,0,4);
server.on("/", handleRequest); // przypisanie funkcji do danego routingu
server.on("/temp", devicesInfo); // przypisanie funkcji do danego routingu
server.on("/set", handleSetDeviceConf);
server.begin(80); // uruchomienie serwera HTTP


  }
}




void sendDataToMyServer(){


  float temp = readTemp();
int valsens = readAnalogVal(A0);

  String url ="http://czujniki.000webhostapp.com/add_from_sensors.php?code=0mx6mqfb&login=maxiges10@gmail.com&num=3";
  String Dev1=url + "&text1=Temp&float="+String(temp,2) ;
    Serial.println();
  Serial.println(Dev1);
http.begin(Dev1);
int respCode = http.GET();
Serial.println(respCode);
http.end();
    String Dev2=url + "&text1=Light&float="+String(valsens) ;
     Serial.println(Dev2);
http.begin(Dev2);
respCode = http.GET();
Serial.println(respCode);
http.end();
}


void senddataToThingworx()
{
 

 // sendDataToMyServer();
  //return;

float temp = readTemp();
int valsens = readAnalogVal(A0);

http.end();
String host = "192.168.10.12";
char URL [] = "/Thingworx/Things/NPSThing/Services/sensorData";
String daneSend  = "";
daneSend  = "{\"Temperature\":\""+String(temp)+"\",\"Grej\":\""+String(valsens)+"\"}";
String content  = "http://192.168.10.12:8080/Thingworx/Things/NPSThing/Services/sensorData?";

Serial.println();



// if(http.begin( content))
// {
  
// http.addHeader("Content-Type", "application/json" , true); 
// http.addHeader("AppKey", "41bfc2c1-2730-4e02-a5cb-4ce6d91eba1b" ); 
// http.addHeader("cache-control", "no-cache" );
// http.addHeader("Accept", "application/json" );


// int kodWysylki = http.PUT(daneSend);
// Serial.println("Kod wysyłki:" + String(kodWysylki)  );


// //String payload = http.getString(); 
// //Serial.println(payload);

// http.end();
// }
// else
// {
//   Serial.println("Czemu sie nie połaczył : Dlaczego ???");
// }




// Serial.println(daneSend);



 // Use WiFiClientSecure class to create TLS connection
  Serial.println(host);
  IPAddress ip = IPAddress(192,168,10,12);
  
  if (!client.connect(ip, 8080)) {
    Serial.println("connection failed");
//TRY AGAIN
          if (!client.connect(ip, 8080)) {
          Serial.println("connection failed");
           return;
         }else{
          Serial.println("Connected to ThingWorx.");
        }
  }else{
    Serial.println("Connected to ThingWorx.");
  }

  String url = "/Thingworx/Things/NPSThing/Services/sensorData";
  Serial.print("requesting URL: ");
  Serial.println(url);

   Serial.print("PUT Value: ");
  Serial.println(daneSend);

  client.print(String("PUT ") + url + " HTTP/1.1\r\n" +
               "Host: " + host+":8080" + "\r\n" +
               "AppKey:41bfc2c1-2730-4e02-a5cb-4ce6d91eba1b" + "\r\n" +
               "x-thingworx-session: false" + "\r\n" +
               "Accept: application/json" + "\r\n" +
               "Connection: close" + "\r\n" +
               "Content-Type: application/json" + "\r\n" +
               "Content-Length: " + String(daneSend.length()) + "\r\n\r\n" +
               daneSend+ "\r\n\r\n");
    
  while (client.connected()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  client.stop();



}